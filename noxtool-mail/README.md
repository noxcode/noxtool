## Spring Boot Mail 邮箱配置

```yaml

#邮箱服务器地址
spring.mail.host=smtp.ym.163.com
#用户名
spring.mail.username=xxxxx@xxxx
#密码
spring.mail.password=xxxxx
#邮件编发格式
spring.mail.default-encoding=UTF-8

#登录服务器是否需要认证
spring.mail.properties.mail.smtp.auth=true
#SSL证书Socket工厂
spring.mail.properties.mail.smtp.socketFactory.class=javax.net.ssl.SSLSocketFactory
#使用SMTPS协议465端口
spring.mail.properties.mail.smtp.socketFactory.port=465

```
