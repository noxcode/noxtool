package com.noxjs.mail;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * @author Nox
 * @Description
 * @date 2020/8/20
 */
@Configuration
public class MailConfiguration {


	@Bean
	@ConditionalOnBean(JavaMailSender.class)
	@ConditionalOnMissingBean(MailService.class)
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	public MailService mailService(JavaMailSender javaMailSender) {
		return new MailService(javaMailSender);
	}

}
