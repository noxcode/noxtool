package com.noxjs.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * @author Nox
 * @Description
 * @date 2020/8/20
 */
@Slf4j
public class MailService {


	private JavaMailSender mailSender;

	public MailService(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * 发送普通邮件
	 *
	 * @param from    发件人
	 * @param to      邮件人
	 * @param subject 主题
	 * @param content 内容
	 */
	public void sendSimpleMail(String from, String to, String subject, String content) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(to);
		message.setFrom(from);
		message.setSubject(subject);
		message.setText(content);
		this.sendSimpleMail(message);
	}

	public void sendSimpleMail(SimpleMailMessage message) {
		try {
			mailSender.send(message);
			log.info("发送邮件成功!，收件人是：{}", message.getFrom());
		} catch (MailException e) {
			e.printStackTrace();
			log.error("邮件发送错误", e);
		}
	}

	/**
	 * 发送网页邮件带别名
	 *
	 * @param from    发件人
	 * @param to      邮件人
	 * @param alias   别名
	 * @param subject 主题
	 * @param content 内容
	 */
	public void sendHtmlMail(String from, String alias, String to, String subject, String content) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setTo(to);
			helper.setFrom(new InternetAddress(from, alias));
			helper.setSubject(subject);
			helper.setText(content, true);
			this.sendMimeMessage(mimeMessage);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("邮件发送错误", e);
		}
	}

	public void sendMimeMessage(MimeMessage message) {
		try {
			mailSender.send(message);
			log.info("发送邮件成功!，收件人是：{}", message.getFrom());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("邮件发送错误", e);
		}
	}

	/**
	 * 发送附件邮件带别名
	 *
	 * @param from     发件人
	 * @param to       邮件人
	 * @param alias    别名
	 * @param subject  主题
	 * @param content  内容
	 * @param filePath 文件地址
	 */
	public void sendAttachmentsMail(String from, String alias, String to, String subject, String content, String filePath) {
		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(new InternetAddress(from, alias));
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(content, true);

			FileSystemResource file = new FileSystemResource(new File(filePath));
			String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
			helper.addAttachment(fileName, file);
			this.sendMimeMessage(message);
			log.info("带附件的邮件已经发送。");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("发送带附件的邮件时发生异常！", e);
		}
	}
}
