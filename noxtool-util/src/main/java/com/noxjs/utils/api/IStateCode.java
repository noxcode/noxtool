package com.noxjs.utils.api;

import java.io.Serializable;

/**
 * @author Nox
 * @Description 业务代码接口
 * @date 2020/8/21
 */
public interface IStateCode extends Serializable {


	/**
	 * 消息
	 *
	 * @return String
	 */
	String getMessage();

	/**
	 * 状态码
	 *
	 * @return int
	 */
	int getCode();

}
