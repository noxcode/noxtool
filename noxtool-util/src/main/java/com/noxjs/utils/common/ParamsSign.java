package com.noxjs.utils.common;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * @Description: 签名验证
 * @author: Nox
 * @date: 2020/1/6
 */
public class ParamsSign {

	private static char[] base64EncodeChars = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

	public ParamsSign() {
	}

	public static String value(TreeMap<String, String> params, String appSecret) {
		try {
			String signStr = "";

			String pName;
			for (Iterator var3 = params.keySet().iterator();
				 var3.hasNext(); signStr = signStr + "&" + pName + "=" + (String) params.get(pName)) {
				pName = (String) var3.next();
			}

			byte[] hmacSha1Bytes = hmacSha1Encrypt(signStr.substring(1), appSecret);
			return URLEncoder.encode(encryptBase64(hmacSha1Bytes), "UTF-8");
		} catch (Exception var5) {
			var5.printStackTrace();
			return "error";
		}
	}

	private static byte[] hmacSha1Encrypt(String encryptText, String encryptKey) throws Exception {
		String macName = "HmacSHA1";
		String encoding = "UTF-8";
		byte[] data = encryptKey.getBytes(encoding);
		SecretKey secretKey = new SecretKeySpec(data, macName);
		Mac mac = Mac.getInstance(macName);
		mac.init(secretKey);
		byte[] text = encryptText.getBytes(encoding);
		return mac.doFinal(text);
	}

	private static String encryptBase64(byte[] key) throws Exception {
		return base64Encode(key);
	}

	private static String base64Encode(byte[] data) {
		StringBuffer sb = new StringBuffer();
		int len = data.length;
		int i = 0;

		while (i < len) {
			int b1 = data[i++] & 255;
			if (i == len) {
				sb.append(base64EncodeChars[b1 >>> 2]);
				sb.append(base64EncodeChars[(b1 & 3) << 4]);
				sb.append("==");
				break;
			}

			int b2 = data[i++] & 255;
			if (i == len) {
				sb.append(base64EncodeChars[b1 >>> 2]);
				sb.append(base64EncodeChars[(b1 & 3) << 4 | (b2 & 240) >>> 4]);
				sb.append(base64EncodeChars[(b2 & 15) << 2]);
				sb.append("=");
				break;
			}

			int b3 = data[i++] & 255;
			sb.append(base64EncodeChars[b1 >>> 2]);
			sb.append(base64EncodeChars[(b1 & 3) << 4 | (b2 & 240) >>> 4]);
			sb.append(base64EncodeChars[(b2 & 15) << 2 | (b3 & 192) >>> 6]);
			sb.append(base64EncodeChars[b3 & 63]);
		}

		return sb.toString();
	}
}
