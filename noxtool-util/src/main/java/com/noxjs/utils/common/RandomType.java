package com.noxjs.utils.common;

/**
 * @author Nox
 * @Description 生成的随机数类型
 * @date 2020/8/21
 */
public enum  RandomType {
	/**
	 * INT STRING ALL
	 */
	INT, STRING, ALL
}
