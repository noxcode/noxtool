package com.noxjs.utils.common;

import cn.hutool.crypto.SecureUtil;

/**
 * @author Nox
 * @Description APP密码加密
 * @date 2020/10/20
 */
public class PassUtil {

	/**
	 * 加密
	 * @param key  key
	 * @param password 密码
	 * @return 返回md5加密
	 */
	public static String encrypt(String key, String password) {
		return SecureUtil.hmacMd5(key).digestHex(password);
	}

	/**
	 * 密码是否匹配
	 * @param key  key
	 * @param rawPassword 原始密码
	 * @param encodedPassword 加密的密码
	 * @return 是/否
	 */
	public static boolean matches(String key, String rawPassword, String encodedPassword) {
		if (rawPassword == null) {
			return false;
		}
		if (encodedPassword == null) {
			return false;
		}
		String pass = SecureUtil.hmacMd5(key).digestHex(rawPassword);
		return pass.contentEquals(encodedPassword);
	}
}
