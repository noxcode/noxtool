## 兼容S3 协议的通用文件存储工具类 ，支持 兼容S3 协议的云存储
   
* MINIO
* 阿里云
* 华为云
* 腾讯云
* 京东云

## 使用方法

```yaml
nox:
  oss:
    #使用云OSS  需要关闭
    path-style-access: false 
    #对应上图 ③ 处配置
    endpoint: s3-cn-east-1.qiniucs.com 
    # 上文创建的AK, 一定注意复制完整不要有空格
    access-key: xxx   
    # 上文创建的SK, 一定注意复制完整不要有空格
    secret-key: xxx   
   # 上文创建的桶名称
    bucketName: xxxx 
```
