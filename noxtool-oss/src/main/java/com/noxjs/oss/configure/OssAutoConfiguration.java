package com.noxjs.oss.configure;

import com.noxjs.oss.http.OssEndpoint;
import com.noxjs.oss.service.OssService;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Nox
 * @Description aws 自动配置类
 * @date 2020/8/20
 */
@AllArgsConstructor
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties({OssProperties.class})
public class OssAutoConfiguration {

	private final OssProperties properties;

	@Bean
	@ConditionalOnMissingBean(OssService.class)
	@ConditionalOnProperty(name = "nox.oss.enable", havingValue = "true", matchIfMissing = true)
	public OssService ossTemplate() {
		return new OssService(properties);
	}

	@Bean
	@ConditionalOnWebApplication
	@ConditionalOnProperty(name = "nox.oss.info", havingValue = "true")
	public OssEndpoint ossEndpoint(OssService service) {
		return new OssEndpoint(service);
	}

}
