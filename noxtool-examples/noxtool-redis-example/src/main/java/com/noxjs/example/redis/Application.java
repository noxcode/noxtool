package com.noxjs.example.redis;

import com.noxjs.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author Nox
 * @Description
 * @date 2020/8/20
 */
@SpringBootApplication
@RestController
public class Application {

	@Autowired
	private RedisService redisService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@RequestMapping("/test")
	public String redis() {
		redisService.setCacheObject("aaa", "bbbb", 300, TimeUnit.SECONDS);
		return "ok";
	}
}
