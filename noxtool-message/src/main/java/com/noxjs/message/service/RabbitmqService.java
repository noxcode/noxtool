package com.noxjs.message.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author Nox
 * @Description
 * @date 2020/8/20
 */
@Slf4j
public class RabbitmqService {

	private final RabbitTemplate rabbitTemplate;

	public RabbitmqService(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	/**
	 * 发送mqtt消息
	 *
	 * @param routingKey 路由key
	 * @param message    消息内容
	 */
	@Async
	public void send(String routingKey, String message) {
		log.info("RabbitmqService send routingKey={},message={} ", routingKey, message);
		rabbitTemplate.convertAndSend(routingKey, message);
	}

	public RabbitTemplate getRabbitTemplate() {
		return rabbitTemplate;
	}
}

