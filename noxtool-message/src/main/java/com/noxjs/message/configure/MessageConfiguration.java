package com.noxjs.message.configure;

import com.noxjs.message.service.RabbitmqService;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Nox
 * @Description
 * @date 2020/8/20
 */
@Configuration
@ConditionalOnBean(RabbitTemplate.class)
@EnableRabbit
public class MessageConfiguration {


	@Bean
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	public RabbitmqService rabbitmqService(RabbitTemplate rabbitTemplate) {
		return new RabbitmqService(rabbitTemplate);
	}
}
